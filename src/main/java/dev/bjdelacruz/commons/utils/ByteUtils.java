package dev.bjdelacruz.commons.utils;

/**
 * This class contains methods for manipulating byte arrays.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public final class ByteUtils {

  /** This class is not to be instantiated by any other class. */
  private ByteUtils() {
    // Empty constructor.
  }

  /**
   * Sign-extends the given <code>byte</code> array. If the most significant (left-most) bit of the
   * first byte in the array is 0, then the array is returned.
   * 
   * @param array The <code>byte</code> array to sign-extend.
   * @param size The size of the new sign-extended <code>byte</code> array.
   * @return A sign-extended <code>byte</code> array of length <code>size</code>, or the original
   * array if the most significant bit is 0.
   */
  public static byte[] signExtendByteArray(byte[] array, int size) {
    if (array == null) {
      throw new IllegalArgumentException("array must not be null");
    }
    if (size <= 0) {
      throw new IllegalArgumentException("size must be greater than 0");
    }
    if (size < array.length) {
      throw new IllegalArgumentException("size must be greater than or equal to length of array");
    }

    if ((array[0] & 0x80) == 0 || size == array.length) {
      return array;
    }

    var newArray = new byte[size];
    for (int idx = 0, idx2 = 0; idx < size; idx++) {
      if (idx >= size - array.length) {
        newArray[idx] = array[idx2++];
      }
      else {
        newArray[idx] = -1;
      }
    }
    return newArray;
  }

}
