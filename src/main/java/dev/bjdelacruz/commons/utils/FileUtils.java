package dev.bjdelacruz.commons.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Utility class that contains methods for reading from and writing to a file. Contains a
 * <code>main</code> method that tests the <code>readFile</code> methods and displays the results in
 * a JFrame.
 * 
 * @author BJ Peter Dela Cruz
 */
public final class FileUtils {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /** Do not instantiate this class. */
  private FileUtils() {
    // Empty constructor.
  }

  /**
   * Writes the given list of strings (lines) to the given file.
   * 
   * @param lines The lines to write to the file.
   * @param file The file to write to.
   * @throws IOException If there are problems writing to the file.
   */
  public static void writeToFile(List<String> lines, File file) throws IOException {
    if (lines == null || lines.isEmpty()) {
      throw new IllegalArgumentException("lines must not be null or empty");
    }
    if (file == null) {
      throw new IllegalArgumentException("file must not be null");
    }

    try (var writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),
            StandardCharsets.UTF_8))) {
      lines.forEach(line -> {
        try {
          writer.write(String.format("%s%n", line));
        }
        catch (IOException ioe) {
          LOGGER.log(Level.SEVERE, ioe.getMessage());
          throw new RuntimeException("A problem was encountered while trying to write to file", ioe);
        }
      });
    }

  }

  /**
   * Loads all the properties (key-value pairs) from a file.
   * 
   * @param properties The <code>Properties</code> object into which to put the properties.
   * @param propertiesFile The properties file from which the properties are loaded.
   * @throws IOException If the properties could not be loaded from the file.
   */
  public static void loadProperties(Properties properties, File propertiesFile) throws IOException {
    if (properties == null) {
      throw new IllegalArgumentException("properties must not be null");
    }
    if (propertiesFile == null) {
      throw new IllegalArgumentException("propertiesFile must not be null");
    }
    try (var istream = new FileInputStream(propertiesFile)) {
      properties.load(istream);
    }
  }

  /**
   * Saves the given property (key-value pair) to the given properties file.
   * 
   * @param key The key.
   * @param value The value.
   * @param propertiesFile The properties file to which the property is saved.
   * @throws IOException If the property could not be saved to the given file.
   */
  public static void saveProperty(String key, String value, File propertiesFile)
      throws IOException {
    if (key == null || key.isEmpty()) {
      throw new IllegalArgumentException("key must not be null or empty");
    }
    if (value == null) {
      throw new IllegalArgumentException("value must not be null");
    }
    if (propertiesFile == null) {
      throw new IllegalArgumentException("propertiesFile must not be null");
    }

    if (!propertiesFile.exists() && !propertiesFile.createNewFile()) {
      throw new IOException("Unable to create " + propertiesFile.getAbsolutePath());
    }

    var properties = new Properties();
    loadProperties(properties, propertiesFile);
    properties.setProperty(key, value);

    try (var ostream = new FileOutputStream(propertiesFile)) {
      var dateCreated = DateTimeFormatter.ofPattern("MMMM dd, yyyy").format(LocalDate.now());
      properties.store(ostream, String.format("Created on %s.", dateCreated));
    }
  }

  /**
   * Gets the value for the given key from the given properties file. Returns <code>null</code> if
   * the key is not found in the file or the file does not exist.
   * 
   * @param key The key whose value is to be retrieved.
   * @param propertiesFile The properties file from which to get the value for the given key.
   * @return The value for the given key, or <code>null</code> if the key is not found in the file
   * or the file does not exist.
   */
  public static Optional<String> getProperty(String key, File propertiesFile) {
    if (key == null || key.isEmpty()) {
      throw new IllegalArgumentException("key must not be null or empty");
    }
    if (propertiesFile == null) {
      throw new IllegalArgumentException("propertiesFile must not be null");
    }

    if (!propertiesFile.exists()) {
      LOGGER.log(Level.INFO, propertiesFile.getAbsolutePath() + " does not exist");
      return Optional.empty();
    }

    try {
      var properties = new Properties();
      loadProperties(properties, propertiesFile);
      return Optional.ofNullable(properties.getProperty(key));
    }
    catch (IOException e) {
      LOGGER.log(Level.SEVERE, e.getMessage());
      return Optional.empty();
    }
  }

  /**
   * Gets the <code>int</code> value for the given key from the given properties file. Returns
   * <code>0</code> if the key is not found in the file, the file does not exist, or the value
   * is not an <code>int</code>.
   * 
   * @param key The key whose value is to be retrieved.
   * @param propertiesFile The properties file from which to get the value for the given key.
   * @return The value for the given key, or <code>0</code> if the key is not found in the file,
   * the file does not exist, or the value is not an <code>int</code>.
   */
  public static int getIntProperty(String key, File propertiesFile) {
    if (key == null || key.isEmpty()) {
      throw new IllegalArgumentException("key must not be null or empty");
    }
    if (propertiesFile == null) {
      throw new IllegalArgumentException("propertiesFile must not be null");
    }

    var value = getProperty(key, propertiesFile);

    try {
      return value.map(Integer::parseInt).orElse(0);
    }
    catch (NumberFormatException nfe) {
      return 0;
    }
  }

  /**
   * Gets the <code>boolean</code> value for the given key from the given properties file. Returns
   * <code>false</code> if the key is not found in the file, the file does not exist, or the value
   * is not a <code>boolean</code>.
   * 
   * @param key The key whose value is to be retrieved.
   * @param propertiesFile The properties file from which to get the value for the given key.
   * @return The value for the given key, or <code>false</code> if the key is not found in the file,
   * the file does not exist, or the value is not a <code>boolean</code>.
   */
  public static boolean getBooleanProperty(String key, File propertiesFile) {
    if (key == null || key.isEmpty()) {
      throw new IllegalArgumentException("key must not be null or empty");
    }
    if (propertiesFile == null) {
      throw new IllegalArgumentException("propertiesFile must not be null");
    }

    var value = getProperty(key, propertiesFile);
    return value.isPresent() && Boolean.parseBoolean(value.get());
  }

  /**
   * Sets the logger to write to the given file.
   * 
   * @param logger The logger.
   * @param logSize The size of the log file.
   * @param logRotationCount The number of log files to use.
   * @param filename The name of the file to which to write logging information.
   */
  public static void configureLogger(Logger logger, int logSize, int logRotationCount,
      String filename) {
    Arrays.stream(logger.getHandlers()).forEach(logger::removeHandler);

    try {
      var fileHandler = new FileHandler(filename, logSize, logRotationCount, true);
      fileHandler.setEncoding(StandardCharsets.UTF_8.name());
      fileHandler.setFormatter(new SimpleFormatter());
      logger.addHandler(fileHandler);
    }
    catch (IOException e) {
      logger.log(Level.SEVERE, e.getMessage());
    }
  }

}
