package dev.bjdelacruz.commons.utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.ColorUIResource;

/**
 * Contains UI-related utility methods.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public final class UiUtils {

  /** Do not instantiate this class. */
  private UiUtils() {
    // Empty constructor.
  }

  /**
   * Displays an error dialog on the EDT.
   * 
   * @param parent The parent component.
   * @param title The title of the error dialog.
   * @param msg The message for the error dialog.
   */
  public static void displayErrorMessage(Component parent, String title,
      String msg) {
    if (title == null || title.isEmpty()) {
      throw new IllegalArgumentException("title must not be null or empty");
    }
    if (msg == null || msg.isEmpty()) {
      throw new IllegalArgumentException("msg must not be null or empty");
    }

    if (SwingUtilities.isEventDispatchThread()) {
      JOptionPane.showMessageDialog(parent, msg, title, JOptionPane.ERROR_MESSAGE);
    }
    else {
      SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(parent, msg, title,
              JOptionPane.ERROR_MESSAGE));
    }
  }

  /**
   * Sets the look-and-feel (L&amp;F) to the system L&amp;F. If an exception is thrown, it is
   * logged, and the application will exit.
   * <p>
   * This method should be called in a static initializer block in the main class of your
   * application.
   * 
   * @param logger The logger to use to log exceptions if problems occur.
   */
  public static void useSystemLookAndFeel(Logger logger) {
    if (logger == null) {
      throw new IllegalArgumentException("logger must not be null");
    }
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (ClassNotFoundException | InstantiationException | IllegalAccessException
           | UnsupportedLookAndFeelException e) {
      logger.log(Level.SEVERE, e.getMessage());
      throw new RuntimeException(e);
    }
  }

  /**
   * Returns true if this application is running on macOS, false otherwise.
   *
   * @return True if this application is running on macOS, false otherwise.
   **/
  public static boolean isRunningOnMacOS() {
    var os = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
    return os.contains("mac") || os.contains("darwin");
  }

  /**
   * Sets the default table cell border color to gray. When an application is running in macOS, no
   * border is drawn between cells in a JTable. Call this method if you want to see a border drawn
   * between cells in your table (just like in Windows).
   * <br /><br />
   * Source: <a href="http://stackoverflow.com/a/13779735/739379">Stack Overflow</a>
   */
  public static void setDefaultTableCellBorderColorMacOS() {
    UIManager.put("Table.gridColor", new ColorUIResource(Color.GRAY));
  }

  /**
   * Sets the ESC key to close the frame that is currently in focus.
   * 
   * @param frame The frame that will be closed when the ESC key is closed.
   */
  public static void setEscKey(JFrame frame) {
    if (frame == null) {
      throw new IllegalArgumentException("frame must not be null");
    }
    var escapeKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);

    var escapeAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        frame.dispose();
      }
    };
    frame.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        .put(escapeKeyStroke, "ESCAPE");
    frame.getRootPane().getActionMap().put("ESCAPE", escapeAction);
  }

  /**
   * Closes the given frame.
   * 
   * @param frame The frame to close.
   */
  public static void closeFrame(JFrame frame) {
    if (frame == null) {
      throw new IllegalArgumentException("frame must not be null");
    }
    frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
  }

}
