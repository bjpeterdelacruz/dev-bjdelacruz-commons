package dev.bjdelacruz.commons.ui;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.swing.AbstractAction;
import javax.swing.JTable;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * An action that will copy one or more selected entries in a table to the clipboard.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 * 
 * @param <E> The type of the entries that are in the table.
 */
public final class CopyToClipboardAction<E> extends AbstractAction implements ClipboardOwner {

  private final JTable table;

  /**
   * Creates a new action that will copy the selected entry or entries to the clipboard.
   * 
   * @param table The table containing the entries to copy.
   */
  public CopyToClipboardAction(JTable table) {
    this(table, "");
  }

  /**
   * Creates a new action that will copy the selected entry or entries to the clipboard.
   * 
   * @param table The table containing the entries to copy.
   * @param title The title for the action.
   */
  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public CopyToClipboardAction(JTable table, String title) {
    super(title);
    Objects.requireNonNull(table);
    this.table = table;
  }

  /** {@inheritDoc} */
  @Override
  @SuppressWarnings("PMD.DataflowAnomalyAnalysis")
  public void actionPerformed(ActionEvent event) {
    @SuppressWarnings("unchecked")
    var model = (CustomTableModel<E>) this.table.getModel();
    var entries = new ArrayList<E>();
    Arrays.stream(table.getSelectedRows()).forEach(row -> entries.add(model.getEntryAt(row)));
    setClipboardContents(entries);
  }

  /** {@inheritDoc} */
  @Override
  public void lostOwnership(Clipboard clipboard, Transferable transferable) {
    // Do nothing.
  }

  /**
   * Sets the contents of the clipboard to the selected entries.
   * 
   * @param entries The list of entries to copy to the clipboard.
   */
  private void setClipboardContents(List<E> entries) {
    var buffer = new StringBuilder();
    for (var index = 0; index < entries.size(); index++) {
      buffer.append(entries.get(index).toString());
      if (index != entries.size() - 1) {
        buffer.append(System.lineSeparator());
      }
    }
    var clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(new StringSelection(buffer.toString()), this);
  }

}
