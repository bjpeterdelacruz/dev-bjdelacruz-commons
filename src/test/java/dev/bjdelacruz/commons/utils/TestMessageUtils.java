package dev.bjdelacruz.commons.utils;

import org.junit.jupiter.api.Test;

import static dev.bjdelacruz.commons.utils.MessageUtils.PROTOCOL_IDENTIFIER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * JUnit tests for the {@link MessageUtils} class.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public class TestMessageUtils {

  /**
   * Tests the {@link MessageUtils#writeProtocolMessage(OutputStream, byte[])} method.
   */
  @Test
  public void testWriteProtocolMessage1() {
    var ostream = new ByteArrayOutputStream();
    MessageUtils.writeProtocolMessage(ostream, new byte[] {0x02, 0x04, 0x06});
    var actual = ostream.toByteArray();
    var expected = new byte[] {PROTOCOL_IDENTIFIER[0], PROTOCOL_IDENTIFIER[1], 0x03, 0x02, 0x04, 0x06};
    assertThat(actual).isEqualTo(expected);
  }

  /**
   * Tests the {@link MessageUtils#writeProtocolMessage(OutputStream)} method.
   */
  @Test
  public void testWriteProtocolMessage2() {
    var ostream = new ByteArrayOutputStream();
    MessageUtils.writeProtocolMessage(ostream);
    var expected = new byte[] {PROTOCOL_IDENTIFIER[0], PROTOCOL_IDENTIFIER[1], 0x01, 0x01};
    assertThat(ostream.toByteArray()).isEqualTo(expected);
  }

  /**
   * Tests the {@link MessageUtils#readProtocolMessage(InputStream)} method.
   */
  @Test
  public void testReadProtocolMessage1() {
    var message = new byte[] {PROTOCOL_IDENTIFIER[0], PROTOCOL_IDENTIFIER[1], 0x03, 0x02, 0x04, 0x06};
    var istream = new ByteArrayInputStream(message);
    var actual = MessageUtils.readProtocolMessage(istream);

    assertThat(actual).isEqualTo(new byte[] {0x02, 0x04, 0x06});
  }

  /**
   * Tests the {@link MessageUtils#readProtocolMessage(InputStream)} method.
   */
  @Test
  public void testReadProtocolMessage2() {
    assertThatThrownBy(() -> MessageUtils.readProtocolMessage(
            new ByteArrayInputStream(new byte[] {(byte) 0xFF})))
            .isInstanceOf(RuntimeException.class);

    assertThatThrownBy(() -> MessageUtils.readProtocolMessage(
            new ByteArrayInputStream(new byte[] {PROTOCOL_IDENTIFIER[0], 0x0F})))
            .isInstanceOf(RuntimeException.class);

    assertThatThrownBy(() -> MessageUtils.readProtocolMessage(
            new ByteArrayInputStream(new byte[] {0x0F, PROTOCOL_IDENTIFIER[1]})))
            .isInstanceOf(RuntimeException.class);

    assertThatThrownBy(() -> MessageUtils.readProtocolMessage(
            new ByteArrayInputStream(
                    new byte[] {PROTOCOL_IDENTIFIER[0], PROTOCOL_IDENTIFIER[0], 0x03, 0x01, 0x02})))
            .isInstanceOf(RuntimeException.class);

    assertThatThrownBy(() -> MessageUtils.readProtocolMessage(
            new ByteArrayInputStream(
                    new byte[] {PROTOCOL_IDENTIFIER[0], PROTOCOL_IDENTIFIER[0], 0x01, 0x01, 0x02})))
            .isInstanceOf(RuntimeException.class);
  }

  /**
   * Tests the {@link MessageUtils#write(DataOutput, Entry)} method.
   * 
   * @throws Exception If there are problems writing the map entries.
   */
  @Test
  @SuppressWarnings("PMD.DataflowAnomalyAnalysis")
  public void testWrite() throws Exception {
    var inputs = new HashMap<String, Object>();
    inputs.put("a", (byte) 0xAF);
    inputs.put("b", (short) 2);
    inputs.put("c", 3);
    inputs.put("d", 4L);
    inputs.put("e", 5.0f);
    inputs.put("f", 6.0);
    inputs.put("g", true);
    inputs.put("h", "c");

    var ostream = new ByteArrayOutputStream();
    try (var out = new ObjectOutputStream(ostream)) {
      for (var entry : inputs.entrySet()) {
        MessageUtils.write(out, entry);
      }
    }

    var outputs = new HashMap<String, Object>();
    try (var in = new ObjectInputStream(new ByteArrayInputStream(ostream.toByteArray()))) {
      for (var index = 0; index < inputs.size(); index++) {
        var pair = MessageUtils.read(in);
        outputs.put(pair.name(), pair.value());
      }
    }

    assertThat(inputs).isEqualTo(outputs);

    ostream = new ByteArrayOutputStream();
    try (var out = new ObjectOutputStream(ostream)) {
      var invalidInputs = Map.of("x", new Object());
      for (var entry : invalidInputs.entrySet()) {
        assertThatThrownBy(() -> MessageUtils.write(out, entry)).isInstanceOf(RuntimeException.class);
      }
    }
  }

  /**
   * Tests the {@link MessageUtils#isSerializedObject(java.io.InputStream)} method.
   * 
   * @throws Exception If there are problems reading or writing the serialized object or byte array.
   */
  @Test
  public void testIsSerializedObject() throws Exception {
    var ostream = new ByteArrayOutputStream();
    var out = new ObjectOutputStream(ostream);
    out.writeObject(new DummyClass());
    out.flush();
    out.close();

    var bytes = ostream.toByteArray();
    assertThat(MessageUtils.isSerializedObject(new ByteArrayInputStream(bytes))).isTrue();

    var message = new byte[] {(byte) 0xAF, (byte) 0xAF, 0x01, 0x02};
    assertThat(!MessageUtils.isSerializedObject(new ByteArrayInputStream(message))).isTrue();
  }

  /**
   * Tests the {@link MessageUtils#writeMessage(DataOutput, Map)} method.
   * 
   * @throws Exception If there are problems writing the message.
   */
  @Test
  public void testWriteMessage() throws Exception {
    var inputMap = new HashMap<String, Object>();
    inputMap.put("a", 1);
    inputMap.put("b", true);
    inputMap.put("c", "cc");

    var ostream = new ByteArrayOutputStream();
    var out = new ObjectOutputStream(ostream);
    MessageUtils.writeMessage(out, inputMap);
    out.close();

    var outputMap = new HashMap<String, Object>();
    try (var in = new ObjectInputStream(new ByteArrayInputStream(ostream.toByteArray()))) {
      var pairs = MessageUtils.readMessage(in);
      for (var pair : pairs) {
        outputMap.put(pair.name(), pair.value());
      }
    }

    assertThat(inputMap).isEqualTo(outputMap);
  }

  /**
   * A simple Serializable class for the JUnit tests in this class.
   * 
   * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
   */
  @SuppressWarnings("PMD.MissingSerialVersionUID")
  private static final class DummyClass implements Serializable {
  }

}
