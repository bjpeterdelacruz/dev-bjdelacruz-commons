/* 
 * @file InterpolatorTest.java 
 * 
 * Created on Jan 20, 2012 
 * 
 * Copyright 2002-2012 Referentia Systems Inc. All Rights Reserved. 
 * 155 Kapalulu Place, Suite 200, Honolulu, Hawaii 96819, U.S.A. 
 * 
 * This software is the confidential and proprietary information 
 * of Referentia Systems Incorporated ("Confidential Information"). 
 * You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement 
 * you entered into with Referentia Systems Incorporated. 
 */
package dev.bjdelacruz.commons.utils.math;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * JUnit tests for the {@link Interpolator} class.
 * 
 * @author BJ Peter Dela Cruz <bdelacruz@referentia.com>
 */
public class TestInterpolator {

  private List<Point<Number, Number>> points;

  /** Sets up the list used in the tests. */
  @BeforeEach
  public void init() {
    points = new ArrayList<>();
    points.add(new Point<>(100, Point.NO_DATA));
    points.add(new Point<>(150, Point.NO_DATA));
    points.add(new Point<>(200, Point.NO_DATA));
    points.add(new Point<>(250, Point.NO_DATA));
    points.add(new Point<>(300, Point.NO_DATA));
    points.add(new Point<>(350, Point.NO_DATA));
    points.add(new Point<>(400, Point.NO_DATA));
    points.add(new Point<>(450, Point.NO_DATA));
    points.add(new Point<>(500, Point.NO_DATA));
  }

  /**
   * Tests the {@link Interpolator#interpolate(Point, Point, Number)} method.
   */
  @Test
  public void interpolatePoint() {
    points.set(2, new Point<>(200, 100.0));
    points.set(4, new Point<>(300, 150.0));
    var actual = Interpolator.interpolate(points.get(2), points.get(4), 250);
    assertThat(actual.getY()).isEqualTo(125.0);

    assertThatThrownBy(() -> Interpolator.interpolate(null, points.get(4), 250))
            .isInstanceOf(IllegalArgumentException.class);
    assertThatThrownBy(() -> Interpolator.interpolate(points.get(2), null, 250))
            .isInstanceOf(IllegalArgumentException.class);
    assertThatThrownBy(() -> Interpolator.interpolate(points.get(2), points.get(4), null))
            .isInstanceOf(IllegalArgumentException.class);
  }

  /**
   * Tests the {@link Interpolator#interpolate(List)} method.
   */
  @Test
  public void interpolateList1() {
    points.set(2, new Point<>(200, 100.0));
    points.set(6, new Point<>(400, 200.0));

    var expected = new ArrayList<>();
    expected.add(new Point<>(100, Point.NO_DATA));
    expected.add(new Point<>(150, Point.NO_DATA));
    expected.add(new Point<>(200, 100.0));
    expected.add(new Point<>(250, 125.0));
    expected.add(new Point<>(300, 150.0));
    expected.add(new Point<>(350, 175.0));
    expected.add(new Point<>(400, 200.0));
    expected.add(new Point<>(450, Point.NO_DATA));
    expected.add(new Point<>(500, Point.NO_DATA));

    assertThat(Interpolator.interpolate(points)).isEqualTo(expected);
  }

  /**
   * Tests the {@link Interpolator#interpolate(List)} method.
   */
  @Test
  public void interpolateList2() {
    assertThat(Interpolator.interpolate(points)).isEqualTo(points);
  }

  /**
   * Tests the {@link Interpolator#interpolate(List)} method.
   */
  @Test
  public void interpolateList3() {
    points.set(2, new Point<>(200, 100.0));
    assertThat(Interpolator.interpolate(points)).isEqualTo(points);
  }

  /**
   * Tests the {@link Interpolator#interpolate(List)} method.
   */
  @Test
  public void interpolateList4() {
    points.set(2, new Point<>(200, 100.0));
    points.set(3, new Point<>(200, 200.0));
    assertThatThrownBy(() -> Interpolator.interpolate(points)).isInstanceOf(IllegalArgumentException.class);
  }

  /**
   * Tests the {@link Interpolator#interpolate(List)} method.
   */
  @Test
  public void interpolateList5() {
    points.set(2, new Point<>(200, 100.0));
    points.set(3, new Point<>(200, Point.NO_DATA));
    points.set(4, new Point<>(250, 200.0));
    assertThatThrownBy(() -> Interpolator.interpolate(points)).isInstanceOf(IllegalArgumentException.class);
  }

  /**
   * Tests the {@link Interpolator#interpolate(List)} method.
   */
  @Test
  public void interpolateList6() {
    points.set(2, new Point<>(200, 100.0));
    points.set(3, new Point<>(250, 200.0));

    var expected = new ArrayList<>();
    expected.add(new Point<>(100, Point.NO_DATA));
    expected.add(new Point<>(150, Point.NO_DATA));
    expected.add(new Point<>(200, 100.0));
    expected.add(new Point<>(250, 200.0));
    expected.add(new Point<>(300, Point.NO_DATA));
    expected.add(new Point<>(350, Point.NO_DATA));
    expected.add(new Point<>(400, Point.NO_DATA));
    expected.add(new Point<>(450, Point.NO_DATA));
    expected.add(new Point<>(500, Point.NO_DATA));

    assertThat(Interpolator.interpolate(points)).isEqualTo(expected);
  }

  /**
   * Tests the {@link Interpolator#interpolate(List)} method.
   */
  @Test
  public void interpolateList7() {
    points.set(0, new Point<>(100, 100.0));
    points.set(8, new Point<>(500, 500.0));

    var expected = new ArrayList<>();
    expected.add(new Point<>(100, 100.0));
    expected.add(new Point<>(150, 150.0));
    expected.add(new Point<>(200, 200.0));
    expected.add(new Point<>(250, 250.0));
    expected.add(new Point<>(300, 300.0));
    expected.add(new Point<>(350, 350.0));
    expected.add(new Point<>(400, 400.0));
    expected.add(new Point<>(450, 450.0));
    expected.add(new Point<>(500, 500.0));

    assertThat(Interpolator.interpolate(points)).isEqualTo(expected);
  }

  /**
   * Tests the {@link Interpolator#interpolate(List)} method.
   */
  @Test
  public void interpolateList8() {
    points.set(0, new Point<>(100, 100.0));
    points.set(3, new Point<>(250, 25.0));
    points.set(6, new Point<>(400, 100.0));
    points.set(8, new Point<>(500, 700.0));

    var expected = new ArrayList<>();
    expected.add(new Point<>(100, 100.0));
    expected.add(new Point<>(150, 75.0));
    expected.add(new Point<>(200, 50.0));
    expected.add(new Point<>(250, 25.0));
    expected.add(new Point<>(300, 50.0));
    expected.add(new Point<>(350, 75.0));
    expected.add(new Point<>(400, 100.0));
    expected.add(new Point<>(450, 400.0));
    expected.add(new Point<>(500, 700.0));

    assertThat(Interpolator.interpolate(points)).isEqualTo(expected);
  }

  /**
   * Tests the {@link Interpolator#mergeAndInterpolate(List)} method.
   */
  @Test
  public void testMergeAndInterpolate1() {
    var points1 = new ArrayList<Point<Number, Number>>();
    points1.add(new Point<>(100, Point.NO_DATA));
    points1.add(new Point<>(200, 25.0));
    points1.add(new Point<>(300, Point.NO_DATA));
    points1.add(new Point<>(500, Point.NO_DATA));

    var points2 = new ArrayList<Point<Number, Number>>();
    points2.add(new Point<>(150, Point.NO_DATA));
    points2.add(new Point<>(250, Point.NO_DATA));
    points2.add(new Point<>(350, 100.0));
    points2.add(new Point<>(400, Point.NO_DATA));
    points2.add(new Point<>(450, 150.0));

    var expected = new ArrayList<>();
    expected.add(new Point<>(100, Point.NO_DATA));
    expected.add(new Point<>(150, Point.NO_DATA));
    expected.add(new Point<>(200, 25.0));
    expected.add(new Point<>(250, 50.0));
    expected.add(new Point<>(300, 75.0));
    expected.add(new Point<>(350, 100.0));
    expected.add(new Point<>(400, 125.0));
    expected.add(new Point<>(450, 150.0));
    expected.add(new Point<>(500, Point.NO_DATA));

    assertThat(Interpolator.mergeAndInterpolate(List.of(points1, points2))).isEqualTo(expected);
  }

  /**
   * Tests the {@link Interpolator#mergeAndInterpolate(List)} method with duplicate values for
   * X.
   */
  @Test
  public void testMergeAndInterpolate3() {
    var points1 = new ArrayList<Point<Number, Number>>();
    points1.add(new Point<>(100, Point.NO_DATA));
    points1.add(new Point<>(200, 25.0));
    points1.add(new Point<>(300, Point.NO_DATA));
    points1.add(new Point<>(500, Point.NO_DATA));

    var points2 = new ArrayList<Point<Number, Number>>();
    points2.add(new Point<>(150, Point.NO_DATA));
    points2.add(new Point<>(200, Point.NO_DATA));
    points2.add(new Point<>(350, Point.NO_DATA));
    points2.add(new Point<>(400, Point.NO_DATA));
    points2.add(new Point<>(450, Point.NO_DATA));

    assertThatThrownBy(() -> Interpolator.mergeAndInterpolate(List.of(points1, points2)))
            .isInstanceOf(IllegalArgumentException.class);
  }
}
