package dev.bjdelacruz.commons.ui;

/**
 * This class contains configuration for a dialog box that displays information about an
 * application (e.g., the name of the application and the author's email).
 *
 * @author BJ Peter Dela Cruz
 */
public final class AboutDialogConfig {
    private final String title;
    private final String version;
    private final String copyright;
    private final String website;
    private final String email;

    /**
     * Creates an instance of this class using an {@link AboutDialogConfigBuilder} object.
     *
     * @param builder Contains the configuration information for a dialog box.
     */
    private AboutDialogConfig(AboutDialogConfigBuilder builder) {
        this.title = builder.title;
        this.version = builder.version;
        this.copyright = builder.copyright;
        this.website = builder.website;
        this.email = builder.email;
    }

    /**
     * Returns the title of the application for the About dialog box.
     *
     * @return The title of the application for the About dialog box.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the version of the application for the About dialog box.
     *
     * @return The version of the application for the About dialog box.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Returns the copyright information for the About dialog box.
     *
     * @return The copyright information for the About dialog box.
     */
    public String getCopyright() {
        return copyright;
    }

    /**
     * Returns the author's website for the About dialog box.
     *
     * @return The author's website for the About dialog box.
     */
    public String getWebsite() {
        return website;
    }

    /**
     * Returns the author's email for the About dialog box.
     *
     * @return The author's email for the About dialog box.
     */
    public String getEmail() {
        return email;
    }

    /**
     * A builder that is used to create an instance of {@link AboutDialogConfig}.
     *
     * @author BJ Peter Dela Cruz
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
    public static final class AboutDialogConfigBuilder {

        private String title;
        private String version;
        private String copyright;
        private String website;
        private String email;

        /**
         * Sets the title and then returns this builder object.
         *
         * @param title The title of the application, must be non-null and not empty.
         * @return This builder object.
         */
        public AboutDialogConfigBuilder title(String title) {
            if (title == null || title.isEmpty()) {
                throw new IllegalArgumentException("title must not be null or empty");
            }
            this.title = title;
            return this;
        }

        /**
         * Sets the version and then returns this builder object.
         *
         * @param version The version of the application, must be non-null and not empty.
         * @return This builder object.
         */
        public AboutDialogConfigBuilder version(String version) {
            if (version == null || version.isEmpty()) {
                throw new IllegalArgumentException("version must not be null or empty");
            }
            this.version = version;
            return this;
        }

        /**
         * Sets the copyright and then returns this builder object.
         *
         * @param copyright The copyright information, must be non-null and not empty.
         * @return This builder object.
         */
        public AboutDialogConfigBuilder copyright(String copyright) {
            if (copyright == null || copyright.isEmpty()) {
                throw new IllegalArgumentException("copyright must not be null or empty");
            }
            this.copyright = copyright;
            return this;
        }

        /**
         * Sets the author's website and then returns this builder object.
         *
         * @param website The application author's website.
         * @return This builder object.
         */
        public AboutDialogConfigBuilder website(String website) {
            this.website = website;
            return this;
        }

        /**
         * Sets the author's email and then returns this builder object.
         *
         * @param email The application author's email.
         * @return This builder object.
         */
        public AboutDialogConfigBuilder email(String email) {
            this.email = email;
            return this;
        }

        /**
         * Creates an instance of {@link AboutDialogConfig} and then returns it.
         *
         * @return An instance of {@link AboutDialogConfig} with all the required parameters set.
         */
        public AboutDialogConfig build() {
            return new AboutDialogConfig(this);
        }
    }
}
