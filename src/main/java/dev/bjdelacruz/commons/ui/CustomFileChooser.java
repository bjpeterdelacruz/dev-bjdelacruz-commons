package dev.bjdelacruz.commons.ui;

import java.awt.Container;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * A custom file chooser that will enable or disable the Open button based on some condition
 * specified by a {@link FileValidator} when the user clicks on a file or enters the name and path
 * of a file.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public final class CustomFileChooser extends JFileChooser {

  private final ExecutorService executor = Executors.newCachedThreadPool();
  private final JButton openButton;
  private final JTextField textField;
  private final FileValidator validator;
  private File selectedFile;

  /**
   * Creates a new CustomFileChooser.
   * 
   * @param validator The validator that will be used to check whether a file is valid based on some
   * condition.
   * @param approveBtnText The text to use for the Open/Save button, may be null.
   * @param fileSelectionMode The file selection mode (e.g. directories only).
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public CustomFileChooser(FileValidator validator, String approveBtnText, int fileSelectionMode) {
    Objects.requireNonNull(validator);
    this.validator = validator;

    if (approveBtnText != null && !approveBtnText.isEmpty()) {
      setApproveButtonText(approveBtnText);
    }

    openButton = getOpenButton(this);
    Objects.requireNonNull(openButton);
    textField = getTextField(this);
    Objects.requireNonNull(textField);

    setFileSelectionMode(fileSelectionMode);
    addPropertyChangeListener(new PropertyChangeListenerImpl());
    textField.addKeyListener(new KeyListenerImpl());
    setSelectedFile(getCurrentDirectory());
  }

  /**
   * A listener that check whether a file is valid if the user clicks on it in the list of files in
   * this file chooser.
   * 
   * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
   */
  private final class PropertyChangeListenerImpl implements PropertyChangeListener {

    /** {@inheritDoc} */
    @Override
    public void propertyChange(PropertyChangeEvent event) {
      if (!JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(event.getPropertyName())) {
        return;
      }

      if (!(event.getNewValue() instanceof File)) {
        return;
      }

      if (selectedFile == null) {
        selectedFile = (File) event.getNewValue();
        executor.execute(new CheckFileTask(selectedFile));
      }
    }

  }

  /**
   * Returns the Open button.
   * 
   * @param container The component that contains other components such as buttons.
   * @return The Open button, or <code>null</code> if none is found.
   */
  private JButton getOpenButton(Container container) {
    if (container == null) {
      throw new IllegalArgumentException("container must not be null");
    }
    for (var index = 0; index < container.getComponentCount(); index++) {
      var comp = container.getComponent(index);
      if (comp instanceof JButton button) {
        if (getApproveButtonText().equals(button.getText())) {
          return button;
        }
      }
      else if (comp instanceof Container con) {
        return getOpenButton(con);
      }
    }
    return null;
  }

  /**
   * Returns the text field in which the user can enter the name and path of a file.
   * 
   * @param container The component that contains other components such as buttons.
   * @return A text field, or <code>null</code> if none is found.
   */
  private static JTextField getTextField(Container container) {
    if (container == null) {
      throw new IllegalArgumentException("container must not be null");
    }
    for (var index = 0; index < container.getComponentCount(); index++) {
      var comp = container.getComponent(index);
      if (comp instanceof JTextField textField) {
        return textField;
      }
      else if (comp instanceof Container con) {
        return getTextField(con);
      }
    }
    return null;
  }

  /**
   * A key listener that will disable the Open button first and then check whether a file is valid.
   * 
   * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
   */
  private final class KeyListenerImpl extends KeyAdapter {

    /** {@inheritDoc} */
    @Override
    public void keyReleased(KeyEvent event) {
      openButton.setEnabled(false);
      if (selectedFile == null) {
        selectedFile = new File(textField.getText());
        executor.execute(new CheckFileTask(selectedFile));
      }
    }

  }

  /**
   * A task that will check whether the given file is valid. A non-EDT thread will check whether the
   * file is valid based on some condition. After the file is checked, the EDT will enable the Open
   * button if the file is valid or disable it if the file is invalid. But the EDT will check first
   * whether the user selected a different file in the meantime. If a different file was selected,
   * that file will be checked before the state of the Open button is updated.
   * 
   * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
   */
  private final class CheckFileTask implements Runnable {

    private final File file;

    /**
     * Creates a new CheckFileTask.
     * 
     * @param file The file to validate.
     */
    CheckFileTask(File file) {
      if (file == null) {
        throw new IllegalArgumentException("file must not be null");
      }
      this.file = file;
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("PMD.NullAssignment")
    public void run() {
      SwingUtilities.invokeLater(() -> {
          File file = new File(textField.getText());
          if (file.equals(CheckFileTask.this.file)) {
            openButton.setEnabled(validator.isValidFile(file));
            selectedFile = null;
          }
          else {
            selectedFile = file;
            executor.execute(new CheckFileTask(file));
          }
      });
    }

  }

}
