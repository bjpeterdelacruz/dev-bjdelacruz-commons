package dev.bjdelacruz.commons.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * A dialog that can be used to tell the user that something is being loaded.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public final class LoadingDialog extends JDialog {

  /**
   * Constructs a new LoadingDialog instance.
   * 
   * @param title The title for this dialog.
   * @param message The message to be displayed in this dialog.
   * @param width The width of this dialog.
   * @param height The height of this dialog.
   * @param font The font for the text in this dialog, may be null.
   * @param iconImage The icon to be displayed for this dialog, may be null.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public LoadingDialog(String title, String message, int width, int height, Font font,
      Image iconImage) {
    if (title == null || title.isEmpty()) {
      throw new IllegalArgumentException("title must not be null or empty");
    }
    if (message == null || message.isEmpty()) {
      throw new IllegalArgumentException("message must not be null or empty");
    }
    if (width <= 0) {
      throw new IllegalArgumentException("width must be greater than 0");
    }
    if (height <= 0) {
      throw new IllegalArgumentException("width must be greater than 0");
    }

    setTitle(title);
    setModal(true);
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setLayout(new BorderLayout());
    setPreferredSize(new Dimension(width, height));
    var label = new JLabel(message);
    if (font != null) {
      label.setFont(font);
    }
    label.setHorizontalAlignment(SwingConstants.CENTER);
    add(label, BorderLayout.CENTER);
    if (iconImage != null) {
      setIconImage(iconImage);
    }
  }

}
