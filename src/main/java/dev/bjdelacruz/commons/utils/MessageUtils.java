package dev.bjdelacruz.commons.utils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.io.DataOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.IntStream;

/**
 * Utility methods for reading and writing a message, which is just an array of bytes.
 * <br />
 * A message can be used to communicate between a client and server. Using a message is preferable
 * to using a serialized object for communication.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public final class MessageUtils {

  private static final byte MOST_RECENT_VERSION = (byte) 0x01;
  private static final byte[] SUPPORTED_VERSIONS;
  static final byte[] PROTOCOL_IDENTIFIER;
  private static final byte[] MESSAGE_IDENTIFIER;
  private static final byte[] SERIALIZED_OBJECT_IDENTIFIER;
  private static final int TWO_BYTES = 2;

  static {
    SUPPORTED_VERSIONS = new byte[1];
    SUPPORTED_VERSIONS[0] = MOST_RECENT_VERSION;

    PROTOCOL_IDENTIFIER = new byte[2];
    PROTOCOL_IDENTIFIER[0] = (byte) 0xAF;
    PROTOCOL_IDENTIFIER[1] = (byte) 0xAF;

    MESSAGE_IDENTIFIER = new byte[2];
    MESSAGE_IDENTIFIER[0] = (byte) 0xBB;
    MESSAGE_IDENTIFIER[1] = (byte) 0xDD;

    SERIALIZED_OBJECT_IDENTIFIER = new byte[2];
    SERIALIZED_OBJECT_IDENTIFIER[0] = (byte) 0xAC;
    SERIALIZED_OBJECT_IDENTIFIER[1] = (byte) 0xED;
  }

  /** Do not instantiate this class. */
  private MessageUtils() {
  }

  /**
   * Writes a protocol message to the given output stream. A default list of supported protocol
   * versions are included in the message.
   * 
   * @param outputStream The output stream to which to write the protocol message.
   */
  public static void writeProtocolMessage(OutputStream outputStream) {
    Objects.requireNonNull(outputStream);
    try {
      outputStream.write(PROTOCOL_IDENTIFIER);
      outputStream.write((byte) SUPPORTED_VERSIONS.length);
      for (var version : SUPPORTED_VERSIONS) {
        outputStream.write(version);
      }
      outputStream.flush();
    }
    catch (IOException ioe) {
      throw new RuntimeException("Unable to write protocol message to stream", ioe);
    }
  }

  /**
   * Writes a protocol message to the given output stream. The given array of supported protocol
   * versions will be included in the message.
   * 
   * @param outputStream The output stream to which to write the protocol message.
   * @param versions An array of bytes representing the protocol versions that are supported.
   */
  public static void writeProtocolMessage(OutputStream outputStream, byte[] versions) {
    Objects.requireNonNull(outputStream);
    Objects.requireNonNull(versions);
    try {
      outputStream.write(PROTOCOL_IDENTIFIER);
      outputStream.write((byte) versions.length);
      for (var version : versions) {
        outputStream.write(version);
      }
      outputStream.flush();
    }
    catch (IOException ioe) {
      throw new RuntimeException("Unable to write protocol message to stream", ioe);
    }
  }

  /**
   * Reads in a protocol message from the given input stream.
   * 
   * @param inputStream The input stream from which to read in a protocol message.
   * @return An array of protocol versions that were in the message.
   */
  public static byte[] readProtocolMessage(InputStream inputStream) {
    Objects.requireNonNull(inputStream);
    try {
      var buf = new byte[2];
      var result = inputStream.read(buf);
      var wrongId = buf[0] != PROTOCOL_IDENTIFIER[0] || buf[1] != PROTOCOL_IDENTIFIER[1];
      if (result != 2 || wrongId) {
        throw new RuntimeException(String.format("Not a protocol message: %s", Arrays.toString(buf)));
      }

      var numVersions = inputStream.read();
      var protocolVersions = new ArrayList<Byte>();
      IntStream.range(0, numVersions).forEach(_ -> {
        try {
          var version = (byte) inputStream.read();
          if (version != -1) {
            protocolVersions.add(version);
          }
        }
        catch (IOException ioe) {
          throw new RuntimeException("A problem was encountered while trying to read in from stream.", ioe);
        }
      });
      if (numVersions != protocolVersions.size() || inputStream.read() != -1) {
        throw new RuntimeException(String.format("Expected %d protocol versions but found %d.",
                numVersions, protocolVersions.size()));
      }

      var array = new byte[protocolVersions.size()];
      var idx = 0;
      for (var version : protocolVersions) {
        array[idx++] = version;
      }
      return array;
    }
    catch (IOException e) {
      throw new RuntimeException("Unable to read protocol message from stream", e);
    }
  }

  /**
   * Writes a message that contains the contents of the given map to the given output stream.
   * 
   * @param outputStream The output stream to which to write the message.
   * @param objectsMap A map whose contents are written to the output stream.
   */
  public static void writeMessage(DataOutput outputStream, Map<String, Object> objectsMap) {
    writeMessage(outputStream, objectsMap, MOST_RECENT_VERSION);
  }

  /**
   * Writes a message that contains the contents of the given map to the given output stream.
   * 
   * @param outputStream The output stream to which to write the message.
   * @param objectsMap A map whose contents are written to the output stream.
   * @param protocolVersion The version of the protocol used.
   */
  public static void writeMessage(DataOutput outputStream, Map<String, Object> objectsMap,
                                  byte protocolVersion) {
    Objects.requireNonNull(outputStream);
    Objects.requireNonNull(objectsMap);
    if (protocolVersion < 0) {
      throw new IllegalArgumentException("protocolVersion is negative.");
    }

    try {
      outputStream.write(MESSAGE_IDENTIFIER); // 2 bytes
      outputStream.write(protocolVersion);

      outputStream.writeInt(objectsMap.keySet().size());
      objectsMap.entrySet().forEach(entry -> write(outputStream, entry));
    }
    catch (IOException ioe) {
      throw new RuntimeException("Unable to write protocol message to stream", ioe);
    }
  }

  /**
   * Writes the given entry (key-value pair) to the given output stream.
   * 
   * @param outputStream The output stream to which to write the key-value pair.
   * @param entry The entry to write to the output stream.
   */
  static void write(DataOutput outputStream, Entry<String, Object> entry) {
    Objects.requireNonNull(outputStream);
    Objects.requireNonNull(entry);
    try {
      outputStream.writeUTF(entry.getKey());

      var o = entry.getValue();
      Objects.requireNonNull(o);
      switch (o) {
          case Byte b -> {
              outputStream.write(DataType.BYTE.number);
              outputStream.writeByte(b);
          }
          case Short s -> {
              outputStream.write(DataType.SHORT.number);
              outputStream.writeShort(s);
          }
          case Integer i -> {
              outputStream.write(DataType.INT.number);
              outputStream.writeInt(i);
          }
          case Long l -> {
              outputStream.write(DataType.LONG.number);
              outputStream.writeLong(l);
          }
          case Float f -> {
              outputStream.write(DataType.FLOAT.number);
              outputStream.writeFloat(f);
          }
          case Double d -> {
              outputStream.write(DataType.DOUBLE.number);
              outputStream.writeDouble(d);
          }
          case Boolean b -> {
              outputStream.write(DataType.BOOLEAN.number);
              outputStream.writeBoolean(b);
          }
          case String s -> {
              outputStream.write(DataType.STRING.number);
              outputStream.writeUTF(s);
          }
          default -> throw new RuntimeException("Type not supported yet: " + o.getClass());
      }
    }
    catch (IOException ioe) {
      throw new RuntimeException("Unable to write entry to stream", ioe);
    }
  }

  /**
   * Reads in a message from the given input stream and returns a list of key-value pairs.
   * 
   * @param inputStream The input stream from which to read in a message.
   * @return The contents of the message as a list of key-value pairs.
   */
  public static List<KeyValuePair> readMessage(ObjectInputStream inputStream) {
    Objects.requireNonNull(inputStream);
    try {
      var buf = new byte[2];
      inputStream.readFully(buf);
      if (buf[0] != MESSAGE_IDENTIFIER[0] && buf[1] != MESSAGE_IDENTIFIER[1]) {
        throw new RuntimeException(String.format("Not a message: %s", Arrays.toString(buf)));
      }
      var version = inputStream.readByte();
      if (version != MOST_RECENT_VERSION) {
        throw new RuntimeException(String.format("Old version of message received: %s != %s", version,
                MOST_RECENT_VERSION));
      }

      var pairs = new ArrayList<KeyValuePair>();

      IntStream.range(0, inputStream.readInt()).forEach(_ -> pairs.add(read(inputStream)));

      return pairs;
    }
    catch (IOException ioe) {
      throw new RuntimeException("A problem was encountered while trying to read from input stream: ", ioe);
    }
  }

  /**
   * Reads in bytes from the given input stream and returns an object containing a key, value, and
   * the type of the value.
   * 
   * @param inputStream The input stream from which to read in bytes.
   * @return An object containing a key, value, and the type of the value.
   */
  @SuppressWarnings("PMD.DataflowAnomalyAnalysis")
  static KeyValuePair read(ObjectInputStream inputStream) {
    Objects.requireNonNull(inputStream);
    try {
      var key = inputStream.readUTF();
      var type = DataType.getDataType(inputStream.read());

      var value = switch (type) {
        case BYTE -> inputStream.readByte();
        case INT -> inputStream.readInt();
        case LONG -> inputStream.readLong();
        case SHORT -> inputStream.readShort();
        case DOUBLE -> inputStream.readDouble();
        case FLOAT -> inputStream.readFloat();
        case BOOLEAN -> inputStream.readBoolean();
        case STRING -> inputStream.readUTF();
      };

      return new KeyValuePair(key, value, type);
    }
    catch (IOException ioe) {
      throw new RuntimeException("A problem was encountered while trying to read from input stream.", ioe);
    }
  }

  /**
   * Reads in the first two bytes from the given input stream and returns <code>true</code> if they
   * are a serialized object identifier or <code>false</code> if they are not.
   * <p>
   * If this method returns <code>true</code>, the caller of this method can choose to read in the
   * object and then ignore it.
   * 
   * @param inputStream The input stream from which to read in bytes.
   * @return <code>true</code> if the bytes that were read in represent a message identifier,
   * <code>false</code> otherwise.
   */
  public static boolean isSerializedObject(InputStream inputStream) {
    Objects.requireNonNull(inputStream);
    var size = 2;
    var pis = new PushbackInputStream(inputStream, size);
    var buffer = new byte[size];
    try {
      if (pis.read(buffer) != TWO_BYTES) {
        return false;
      }
      var flag = buffer[0] == SERIALIZED_OBJECT_IDENTIFIER[0] && buffer[1] == SERIALIZED_OBJECT_IDENTIFIER[1];
      pis.unread(buffer);
      return flag;
    }
    catch (IOException ioe) {
      throw new RuntimeException("An error was encountered while determining whether input stream contains "
              + "a serialized object", ioe);
    }
  }

  /**
   * Represents the type for an object.
   * 
   * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
   */
  @SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_FIELD_NAMES")
  public enum DataType {
    /** Byte. */
    BYTE(0),
    /** Integer. */
    INT(1),
    /** Long. */
    LONG(2),
    /** Short. */
    SHORT(3),
    /** Double. */
    DOUBLE(4),
    /** Float. */
    FLOAT(5),
    /** Boolean. */
    BOOLEAN(6),
    /** String. */
    STRING(7);
    private final byte number;

    /**
     * Creates a new DataType enum.
     * 
     * @param number A number that is associated with this enum.
     */
    DataType(int number) {
      this.number = (byte) number;
    }

    /**
     * Gets the <code>DataType</code> that is associated with the given number. Throws a
     * <code>RuntimeException</code> if the number is not associated with any <code>DataType</code>.
     * 
     * @param number The number associated with a <code>DataType</code>.
     * @return A <code>DataType</code> enum.
     */
    public static DataType getDataType(int number) {
      return Arrays.stream(values()).filter(type -> type.number == number).findFirst().orElseThrow(
              () -> new RuntimeException(String.format("Unable to find data type for %d", number)));
    }
  }

}
