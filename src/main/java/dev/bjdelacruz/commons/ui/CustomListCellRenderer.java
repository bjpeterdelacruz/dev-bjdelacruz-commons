package dev.bjdelacruz.commons.ui;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * A renderer that displays a blue background when a mouse hovers over an item in the list.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 * 
 * @param <E> A Label object.
 */
public class CustomListCellRenderer<E extends ListCellLabel> extends JLabel implements
    ListCellRenderer<ListCellLabel> {

  /**
   * Creates a new renderer with no transparency.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public CustomListCellRenderer() {
    setOpaque(true);
  }

  /** {@inheritDoc} */
  @Override
  public Component getListCellRendererComponent(JList<? extends ListCellLabel> list, ListCellLabel value,
                                                int index, boolean isSelected, boolean cellHasFocus) {
    setText(value.getDisplayName());
    if (isSelected) {
      setBackground(list.getSelectionBackground());
      setForeground(list.getSelectionForeground());
    }
    else {
      setBackground(list.getBackground());
      setForeground(list.getForeground());
    }
    return this;
  }

}
