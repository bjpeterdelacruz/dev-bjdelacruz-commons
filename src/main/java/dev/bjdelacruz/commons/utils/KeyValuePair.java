package dev.bjdelacruz.commons.utils;

import java.util.Objects;

/**
 * Represents a key-value pair.
 *
 * @param name  The key.
 * @param value The value.
 * @param type  The type of the value.
 *
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public record KeyValuePair(String name, Object value, MessageUtils.DataType type) {

  /**
   * Creates a new KeyValuePair.
   */
  public KeyValuePair {
    if (name == null || name.isEmpty()) {
      throw new IllegalArgumentException("name must not be null or empty");
    }
    if (value == null) {
      throw new IllegalArgumentException("value must not be null");
    }
    if (type == null) {
      throw new IllegalArgumentException("type must not be null");
    }
  }

  /**
   * Returns the key.
   *
   * @return The key.
   */
  @Override
  public String name() {
    return name;
  }

  /**
   * Returns the value.
   *
   * @return The value.
   */
  @Override
  public Object value() {
    return value;
  }

  /**
   * Returns the type of the value. See {@link dev.bjdelacruz.commons.utils.MessageUtils.DataType}.
   *
   * @return The type of the value.
   */
  @Override
  public MessageUtils.DataType type() {
    return type;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object object) {
    if (!(object instanceof KeyValuePair pair)) {
      return false;
    }
    return name.equals(pair.name) && value.equals(pair.value) && type == pair.type;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return Objects.hash(name, value, type);
  }

}
