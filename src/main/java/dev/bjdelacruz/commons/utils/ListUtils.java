package dev.bjdelacruz.commons.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * Contains list-related utility methods.
 * 
 * @author BJ Peter Dela Cruz
 */
public final class ListUtils {

  /** Do not instantiate this class. */
  private ListUtils() {
    // Empty constructor.
  }

  /**
   * <p>Converts a list of numbers into a string representation separated by commas.</p>
   * 
   * If <code>concatenateSequence</code> is <code>true</code> and the list contains a sequence (
   * <code>n_1, n_2, n_3, ..., n_x</code>) and the sequence is sorted in increasing order (
   * <code>n_1 &lt; n_2  &lt;  n_3  &lt;  ...  &lt;  n_x</code>) and each number in the sequence is
   * one whole number greater than the previous number (
   * <code>n_1, n_1 + 1, n_1 + 2, ..., n_1 + x</code>), the first and last numbers in the sequence
   * will appear in the string, separated by a hyphen. For example:<br>
   * <br>
   * 
   * <table>
   * <caption>Converting list of integers into string representation separated by commas.</caption>
   * <tr>
   * <td style="text-align: right"><u>Input</u></td>
   * <td colspan="2">1 3 5 6 7 9 11 13 14 15 17 19</td>
   * </tr>
   * <tr>
   * <td><u>Output</u></td>
   * <td colspan="2">1, 3, 5-7, 9, 11, 13-15, 17, 19</td>
   * </tr>
   * <tr>
   * <td><u>Output</u></td>
   * <td>1, 3, 5, 6, 7, 9, 11, 13, 14, 15, 17, 19</td>
   * <td>(if <code>concatenateSequence</code> is false)</td>
   * </tr>
   * </table>
   * 
   * @param list The list to print to a string.
   * @param concatenateSequence True to concatenate all sequences (e.g. 1, 2, 3 -&gt; 1-3), false
   * to not concatenate them.
   * @return The string representation of the list.
   */
  public static String integersListToString(List<Integer> list, boolean concatenateSequence) {
    if (list == null || list.isEmpty()) {
      return "";
    }

    var buffer = new StringBuilder();
    for (int start = 0, end = 1; end <= list.size(); end++) {
      var count = 0;
      while (concatenateSequence && end < list.size()) {
        if (list.get(end) - list.get(start) != end - start) {
          break;
        }
        end++;
        count++;
      }

      String temp;
      if (count == 0) {
        temp = list.get(start) + ", ";
      }
      else {
        temp = list.get(start) + "-" + list.get(end - 1) + ", ";
      }
      buffer.append(temp);
      start = end;
    }

    return buffer.substring(0, buffer.toString().lastIndexOf(','));
  }

  /**
   * Creates a set with the given list of items in it.
   * 
   * @param <E> The type of the items.
   * @param items The items to put in a set.
   * @return A set with the given items in it.
   */
  @SafeVarargs
  public static <E> Set<E> createSet(E... items) {
    if (items == null) {
      throw new IllegalArgumentException("items must not be null");
    }
    if (items.length == 0) {
      return new HashSet<>();
    }
    return new HashSet<>(List.of(items));
  }

  /**
   * Creates a list of integers from N to M, inclusive.
   * 
   * @param begin The least integer that will be in the list.
   * @param end The greatest integer that will be in the list.
   * @return A list of integers from begin to end, inclusive.
   */
  public static List<Integer> createIntegersList(int begin, int end) {
    if (begin > end) {
      throw new IllegalArgumentException("begin must be less than or equal to end");
    }
    return IntStream.rangeClosed(begin, end).boxed().toList();
  }

}
