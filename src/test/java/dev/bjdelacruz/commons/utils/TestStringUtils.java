package dev.bjdelacruz.commons.utils;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit tests for the {@link StringUtils} class.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public class TestStringUtils {

  /**
   * Tests the {@link StringUtils#replaceLast(String, String, String)} method.
   */
  @Test
  public void testReplaceLast() {
    assertThat(StringUtils.replaceLast("hellohellohello", "ello", "i")).isEqualTo("hellohellohi");
    assertThat(StringUtils.replaceLast(null, "hello", "i")).isEmpty();
    assertThat(StringUtils.replaceLast("hello", null, "i")).isEqualTo("hello");
    assertThat(StringUtils.replaceLast("helloo", "ello", null)).isEqualTo("helloo");
    assertThat(StringUtils.replaceLast("hellooo", "i", "ey")).isEqualTo("hellooo");
  }

  /**
   * Tests the {@link StringUtils#toCharacterList(String)} method.
   */
  @Test
  public void testToCharacterList() {
    assertThat(StringUtils.toCharacterList(null)).isEqualTo(new ArrayList<Character>());
    assertThat(StringUtils.toCharacterList("")).isEqualTo(new ArrayList<Character>());
    var chars = new ArrayList<>(List.of('k', 'o', 'n', 'g', 'o', 'u'));
    assertThat(StringUtils.toCharacterList("kongou")).isEqualTo(chars);
  }

  /**
   * Tests the {@link StringUtils#toString(List)} method.
   */
  @Test
  public void testToString() {
    assertThat(StringUtils.toString(null)).isEmpty();
    assertThat(StringUtils.toString(new ArrayList<>())).isEmpty();
    assertThat(StringUtils.toString(List.of('k', 'o', 'n', 'g', 'o', 'u'))).isEqualTo(
        "kongou");
  }

  /**
   * Tests the {@link StringUtils#getListContents(List)} method.
   */
  @Test
  public void testGetListContents() {
    assertThat(StringUtils.getListContents(null)).isEqualTo("[]");
    assertThat(StringUtils.getListContents(new ArrayList<Character>())).isEqualTo("[]");
    var chars = List.of('k', 'o', 'n', 'g', 'o', 'u');
    assertThat(StringUtils.getListContents(chars)).isEqualTo("[k, o, n, g, o, u]");
  }

  /**
   * Tests the {@link StringUtils#get2dArrayContents(Object[][])} method.
   */
  @Test
  @SuppressWarnings("PMD.DataflowAnomalyAnalysis")
  public void testGet2dArrayContents1() {
    assertThat(StringUtils.get2dArrayContents((Object[][]) null)).isEqualTo("[]");
    var obj2 = new Object[0][];
    assertThat(StringUtils.get2dArrayContents(obj2)).isEqualTo("[]");

    var array = new Integer[3][];
    array[0] = List.of(1, 2, 3).toArray(new Integer[3]);
    array[1] = List.of(3, 6, 9).toArray(new Integer[3]);
    array[2] = List.of(10, 20, 30).toArray(new Integer[3]);

    var expected = "[ [1, 2, 3]\n  [3, 6, 9]\n  [10, 20, 30] ]";

    assertThat(StringUtils.get2dArrayContents(array)).isEqualTo(expected);
  }

  /**
   * Tests the {@link StringUtils#get2dArrayContents(List)} method.
   */
  @Test
  @SuppressWarnings("PMD.DataflowAnomalyAnalysis")
  public void testGet2dArrayContents2() {
    assertThat(StringUtils.get2dArrayContents((List<Integer[][]>) null)).isEqualTo("[]");
    var list2 = new ArrayList<>();
    assertThat(StringUtils.get2dArrayContents(list2)).isEqualTo("[]");

    var array1 = new Integer[3][];
    array1[0] = List.of(1, 2, 3).toArray(new Integer[3]);
    array1[1] = List.of(3, 6, 9).toArray(new Integer[3]);
    array1[2] = List.of(10, 20, 30).toArray(new Integer[3]);

    var array2 = new Integer[3][];
    array2[0] = List.of(1, 2, 3).toArray(new Integer[3]);
    array2[1] = List.of(3, 6, 9).toArray(new Integer[3]);
    array2[2] = List.of(10, 20, 30).toArray(new Integer[3]);

    var arrays = List.of(array1, array2);

    var expected = "[ [1, 2, 3]\n  [3, 6, 9]\n  [10, 20, 30] ]";
    expected = expected + "\n\n" + expected;

    assertThat(StringUtils.get2dArrayContents(arrays)).isEqualTo(expected);
  }

  /**
   * Tests the {@link StringUtils#getContents(List, boolean)} method.
   */
  @Test
  public void testGetContents() {
    assertThat(StringUtils.getContents(null, false)).isEmpty();
    assertThat(StringUtils.getContents(new ArrayList<>(), false)).isEmpty();
    assertThat(StringUtils.getContents(List.of("Alice"), false)).isEqualTo("Alice");

    var people = List.of("Alice", "Charlie", "Bob", "David");
    var expected = """
            ------------------------------------------------------------
            Alice    Charlie  Bob      David   \s
            ------------------------------------------------------------
            """;
    assertThat(StringUtils.getContents(people, false)).isEqualTo(expected);
    expected = """
            ------------------------------------------------------------
            Alice    Bob      Charlie  David   \s
            ------------------------------------------------------------
            """;
    assertThat(StringUtils.getContents(new ArrayList<>(people), true)).isEqualTo(expected);
  }
}
