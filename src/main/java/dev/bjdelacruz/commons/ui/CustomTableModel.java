package dev.bjdelacruz.commons.ui;

import javax.swing.table.AbstractTableModel;

/**
 * An interface that contains additional useful methods for table models.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 * 
 * @param <E> The type of the entries that are stored in the table model.
 */
public abstract class CustomTableModel<E> extends AbstractTableModel {

  /**
   * Adds the given entry to the table model.
   * 
   * @param entry The entry to add to the table model.
   */
  public abstract void addEntry(E entry);

  /**
   * Gets the entry in the table model at the given row.
   * 
   * @param row The row in the table model.
   * @return An entry in the table model at the given row.
   */
  public abstract E getEntryAt(int row);

  /**
   * Returns the row index that contains the given entry or -1 if the entry does not exist in the
   * table model.
   * 
   * @param entry The entry in the table model.
   * @return The row index that contains the given entry or -1 if the entry does not exist in the
   * table model.
   */
  public abstract int getRowIndexOf(E entry);

}
