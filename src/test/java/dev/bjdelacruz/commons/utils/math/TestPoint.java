package dev.bjdelacruz.commons.utils.math;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Unit tests for the {@link Point} class.
 *
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public class TestPoint {

    /**
     * Tests the constructors and methods in the {@link Point} class.
     */
    @Test
    public void testPoint() {
        var point = new Point<>(5, 10);
        assertThat(point).isEqualTo(new Point<>(point));
        assertThat(point.hashCode()).isEqualTo(new Point<>(point).hashCode());

        assertThat(point).isNotEqualTo(new Point<>(5, 8));
        assertThat(point).isNotEqualTo(new Point<>(8, 10));

        assertThatThrownBy(() -> new Point<>(null, 10)).isInstanceOf(NullPointerException.class);
        assertThatThrownBy(() -> new Point<>(5, null)).isInstanceOf(NullPointerException.class);

        assertThat(point.equals(new Object())).isFalse();
    }
}
