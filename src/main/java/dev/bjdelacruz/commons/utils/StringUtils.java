package dev.bjdelacruz.commons.utils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.IntStream;

/**
 * This class contains utility methods for string manipulation.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
@SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_CLASS_NAMES")
public final class StringUtils {

  private static final int MAX_WIDTH_OF_ROW = 60;
  private static final int NUMBER_OF_SPACES_BETWEEN_STRINGS = 2;

  /** Do not instantiate this class. */
  private StringUtils() {
    // Empty constructor.
  }

  /**
   * Replaces the last occurrence of a substring in a string with a new substring. For example:
   * <br />
   * <code>replaceLast("hellohellohello", "ello", "i")</code> returns <code>"hellohellohi"</code>
   * 
   * @param string The string that contains the substring to replace.
   * @param oldSubstring The substring to replace.
   * @param newSubstring The new substring to insert in place of the old substring.
   * @return A new string with the last occurrence of the old substring replaced.
   */
  public static String replaceLast(String string, String oldSubstring, String newSubstring) {
    if (string == null) {
      return "";
    }
    if (oldSubstring == null || newSubstring == null) {
      return string;
    }
    if (string.lastIndexOf(oldSubstring) == -1) {
      return string;
    }
    return string.substring(0, string.lastIndexOf(oldSubstring)) + newSubstring;
  }

  /**
   * Given a string, returns a list of characters in the string.
   * 
   * @param string The string from which to extract characters.
   * @return The list of characters in the string.
   */
  public static List<Character> toCharacterList(String string) {
    if (string == null || string.isEmpty()) {
      return new ArrayList<>();
    }

    var letters = new ArrayList<Character>();
    IntStream.range(0, string.length()).forEach(i -> letters.add(string.charAt(i)));
    return letters;
  }

  /**
   * Returns a string with all the characters in the list concatenated together.
   * 
   * @param letters List of characters.
   * @return A string containing all the characters concatenated together.
   */
  public static String toString(List<Character> letters) {
    if (letters == null || letters.isEmpty()) {
      return "";
    }

    var sj = new StringJoiner("");
    letters.forEach(c -> sj.add(String.valueOf(c)));
    return sj.toString();
  }

  /**
   * Gets the contents of a list of objects (strings, integers, etc.).
   * 
   * @param list The list of objects.
   * @return A string displaying the contents of a list.
   */
  public static String getListContents(List<?> list) {
    if (list == null || list.isEmpty()) {
      return "[]";
    }

    var sj = new StringJoiner(", ", "[", "]");
    IntStream.range(0, list.size()).forEach(i -> sj.add(list.get(i).toString()));
    return sj.toString();
  }

  /**
   * Returns a string representing the contents of the given 2D array in a readable format.
   * 
   * @param <E> The type of the items in the 2D array.
   * @param array The 2D array to print.
   * @return A string representing the contents of the given 2D array.
   */
  public static <E> String get2dArrayContents(E[][] array) {
    if (array == null || array.length == 0) {
      return "[]";
    }

    var sj = new StringJoiner("\n  ", "[ ", " ]");
    IntStream.range(0, array.length).forEach(i -> sj.add(getListContents(Arrays.asList(array[i]))));
    return sj.toString();
  }

  /**
   * Given a list of 2D arrays of objects, returns a string representing the contents of each 2D
   * array in a readable format.
   * 
   * @param list List of 2D arrays of objects.
   * @return A string representing the contents of each 2D array of objects.
   */
  public static String get2dArrayContents(List<?> list) {
    if (list == null || list.isEmpty()) {
      return "[]";
    }

    var numTables = 0;

    var buffer = new StringBuilder();
    buffer.append("[ ");

    for (var o : list) {
      if (o instanceof Object[][] objects) {
        IntStream.range(0, objects.length - 1)
                .forEach(i -> buffer.append(getListContents(List.of(objects[i]))).append("\n  "));
        buffer.append(getListContents(Arrays.asList(objects[objects.length - 1])));
      }
      buffer.append(" ]");
      numTables++;
      if (numTables < list.size()) {
        buffer.append("\n\n[ ");
      }
    }

    return buffer.toString();
  }

  /**
   * Returns the contents of the given list as a string. Good for debugging.
   * 
   * @param strings A list of strings.
   * @param shouldSort True to sort the list in alphabetical order before retrieving its contents,
   * false otherwise.
   * @return The contents of the given list.
   */
  public static String getContents(List<String> strings, boolean shouldSort) {
    if (strings == null || strings.isEmpty()) {
      return "";
    }
    if (strings.size() == 1) {
      return strings.getFirst();
    }

    if (shouldSort) {
      strings.sort(String::compareTo);
    }

    var table = new StringBuilder();
    var max = strings.stream().mapToInt(String::length).max().orElse(strings.getFirst().length());
    for (var idx = 0; idx < strings.size(); idx++) {
      var numberOfColumns = getNumberOfColumns(max);
      if ((idx + 1) % numberOfColumns == 0) {
        table.append(getColumn(strings.get(idx), max)).append("\n");
      }
      else {
        table.append(getColumn(strings.get(idx), max + NUMBER_OF_SPACES_BETWEEN_STRINGS));
      }
      if ((idx + 1) % numberOfColumns != 0 && idx + 1 == strings.size()) {
        table.append("\n");
      }
    }

    return getHyphens() + table + getHyphens();
  }

  /**
   * Returns a string containing hyphens.
   *
   * @return A string containing hyphens.
   */
  private static String getHyphens() {
    var sj = new StringJoiner("", "", "\n");
    IntStream.range(0, MAX_WIDTH_OF_ROW).forEach(_ -> sj.add("-"));
    return sj.toString();
  }

  /**
   * Returns the number of columns that should be printed to the screen.
   *
   * @param maxLength The maximum length of a string in a column.
   * @return The number of columns.
   */
  private static int getNumberOfColumns(int maxLength) {
    if (maxLength <= 0) {
      throw new IllegalArgumentException("maxLength must be greater than 0");
    }

    var numColumns = 0;
    var length = maxLength + NUMBER_OF_SPACES_BETWEEN_STRINGS;
    while (length * numColumns + maxLength < MAX_WIDTH_OF_ROW) {
      numColumns++;
    }
    return numColumns;
  }

  /**
   * Returns a string containing the given string with zero, one, or more spaces appended to the
   * end of it.
   *
   * @param string The string to be printed.
   * @param maxCharacterCount The maximum number of characters for this column. If the number of
   * characters in the string is less than this number, spaces will be appended to end of the
   * string until the length of the new string is equal to this number. Otherwise, the string is
   * returned with no spaces appended.
   * @return A string containing the string and spaces.
   */
  private static String getColumn(String string, int maxCharacterCount) {
    if (string == null || string.isEmpty()) {
      throw new IllegalArgumentException("string must not be null or empty");
    }
    if (maxCharacterCount <= 0) {
      throw new IllegalArgumentException("maxCharacterCount must be greater than 0");
    }

    var sj = new StringJoiner("", string, "");
    IntStream.range(string.length(), maxCharacterCount).forEach(_ -> sj.add(" "));
    return sj.toString();
  }
}
