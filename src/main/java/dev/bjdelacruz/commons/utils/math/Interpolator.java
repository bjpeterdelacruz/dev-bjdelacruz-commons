package dev.bjdelacruz.commons.utils.math;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A utility class that contains methods for performing linear interpolation.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public final class Interpolator {

  /** Don't instantiate this class. */
  private Interpolator() {
    // Empty constructor.
  }

  /**
   * Interpolates a point using the linear interpolation formula:<br>
   * <br />
   * 
   * <code><b>Y = left_y + ((X - left_x) * (right_y - left_y) / (right_x - left_x))</b></code><br />
   * <br />
   * 
   * where <code>Y</code> is an interpolated value at <code>X</code>.
   * 
   * @param left The left data point (left_x, left_y).
   * @param right The right data point (right_x, right_y).
   * @param x The value used to calculate Y.
   * @return A {@link Point} with an interpolated value for Y.
   */
  public static Point<Number, Number> interpolate(Point<Number, Number> left,
      Point<Number, Number> right, Number x) {
    if (left == null) {
      throw new IllegalArgumentException("left must not be null");
    }
    if (right == null) {
      throw new IllegalArgumentException("right must not be null");
    }
    if (x == null) {
      throw new IllegalArgumentException("x must not be null");
    }
    var deltaX = right.getX().longValue() - left.getX().longValue();
    var deltaY = right.getY().doubleValue() - left.getY().doubleValue();

    var diff = x.longValue() - left.getX().longValue();

    return new Point<>(x, left.getY().doubleValue() + (diff * deltaY / deltaX));
  }

  /**
   * Interpolates all the points in the given list that are between pairs of points that contain
   * a value for Y.
   * <br /><br />
   *
   * If the list contains fewer than two points that contain a value for Y, then the original list is
   * returned.
   *
   * @param points A list of {@link Point} objects.
   * @throws IllegalArgumentException If the list contains duplicate values for X.
   * @return A list of {@link Point} objects with interpolated values for Y.
   */
  public static List<Point<Number, Number>> interpolate(List<Point<Number, Number>> points) {
    var count = points.stream().map(point -> point.getX().intValue()).collect(Collectors.toSet());
    if (count.size() != points.size()) {
      throw new IllegalArgumentException("List contains duplicate values for X.");
    }

    var copy = new ArrayList<>(points);

    // get the index of each point that contains a value for Y
    var indexes =
            IntStream.range(0, copy.size()).filter(
                    idx -> !copy.get(idx).getY().equals(Point.NO_DATA)).boxed().toList();

    // interpolate the value for each point between two points that contain a value for Y
    for (var idx = 0; idx + 1 < indexes.size(); idx++) {
      var left = indexes.get(idx);
      var right = indexes.get(idx + 1);
      var current = left + 1;
      while (current != right) {
        copy.set(current, interpolate(copy.get(left), copy.get(right), copy.get(current).getX()));
        current++;
      }
    }

    return copy;
  }

  /**
   * Merges multiple lists together and then interpolates all the missing points in the merged list.
   *
   * @param lists A list of lists of {@link Point} objects.
   * @throws IllegalArgumentException If the merged list contains duplicate values for X.
   * @return A list of {@link Point} objects containing all the points from both lists.
   */
  public static List<Point<Number, Number>> mergeAndInterpolate(List<List<Point<Number, Number>>> lists) {
    if (lists == null) {
      throw new IllegalArgumentException("lists must not be null");
    }

    var mergedList = new ArrayList<Point<Number, Number>>();
    lists.forEach(mergedList::addAll);

    mergedList.sort(Comparator.comparing(point -> BigDecimal.valueOf(point.getX().doubleValue())));

    return interpolate(mergedList);
  }
}
