package dev.bjdelacruz.commons.utils;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * JUnit tests for the {@link ByteUtils} class.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public class TestByteUtils {

  /** Tests the {@link ByteUtils#signExtendByteArray(byte[], int)} method. */
  @Test
  public void signExtendByteArray1() {
    byte[] actual = ByteUtils.signExtendByteArray(new byte[] {0, 8}, 4);
    assertThat(actual[0]).isEqualTo((byte) 0);
    assertThat(actual[1]).isEqualTo((byte) 8);
    assertThat(actual.length).isEqualTo(2);

    actual = ByteUtils.signExtendByteArray(new byte[] {2, 8}, 2);
    assertThat(actual[0]).isEqualTo((byte) 2);
    assertThat(actual[1]).isEqualTo((byte) 8);
    assertThat(actual.length).isEqualTo(2);

    actual = ByteUtils.signExtendByteArray(new byte[] {-1, 8}, 2);
    assertThat(actual[0]).isEqualTo((byte) -1);
    assertThat(actual[1]).isEqualTo((byte) 8);
    assertThat(actual.length).isEqualTo(2);
  }

  /** Tests the {@link ByteUtils#signExtendByteArray(byte[], int)} method. */
  @Test
  public void signExtendByteArray2() {
    var actual = ByteUtils.signExtendByteArray(new byte[] {-1, 16}, 4);
    assertThat(actual[0]).isEqualTo((byte) -1);
    assertThat(actual[1]).isEqualTo((byte) -1);
    assertThat(actual[2]).isEqualTo((byte) -1);
    assertThat(actual[3]).isEqualTo((byte) 16);
    assertThat(actual.length).isEqualTo(4);

    assertThatThrownBy(() -> ByteUtils.signExtendByteArray(null, 2))
            .isInstanceOf(IllegalArgumentException.class);
    assertThatThrownBy(() -> ByteUtils.signExtendByteArray(new byte[] {-1, -1, -1, 32}, 0))
            .isInstanceOf(IllegalArgumentException.class);
    assertThatThrownBy(() -> ByteUtils.signExtendByteArray(new byte[] {-1, -1, -1, 32}, -1))
            .isInstanceOf(IllegalArgumentException.class);
    assertThatThrownBy(() -> ByteUtils.signExtendByteArray(new byte[] {-1, -1, -1, 32}, 2))
            .isInstanceOf(IllegalArgumentException.class);
  }
}
