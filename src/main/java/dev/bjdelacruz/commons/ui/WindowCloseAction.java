package dev.bjdelacruz.commons.ui;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.util.Objects;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * An action that will close the given frame.
 * 
 * @author BJ Peter Dela Cruz
 */
public final class WindowCloseAction extends AbstractAction {

  private final JFrame frame;

  /**
   * Creates a new CloseAction.
   * 
   * @param frame The frame to close.
   * @param title The title for this action.
   */
  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public WindowCloseAction(JFrame frame, String title) {
    super(title == null ? "" : title);
    Objects.requireNonNull(frame);
    this.frame = frame;
  }

  /** {@inheritDoc} */
  @Override
  public void actionPerformed(ActionEvent event) {
    frame.dispatchEvent(new WindowEvent(this.frame, WindowEvent.WINDOW_CLOSING));
  }

}
