package dev.bjdelacruz.commons.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import dev.bjdelacruz.commons.utils.UiUtils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * An action that will export the data in a table to a file.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 * 
 * @param <E> The type of the entries in the table.
 */
public final class ExportAction<E extends Serializable> implements ActionListener {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private final JTable table;
  private final byte[] magicCookie;

  /**
   * Creates a new ExportAction.
   * 
   * @param table The table that contains data to export to file.
   * @param magicCookie The magic cookie used to distinguish the output file from other files.
   */
  @SuppressFBWarnings("EI_EXPOSE_REP2")
  public ExportAction(JTable table, byte[] magicCookie) {
    Objects.requireNonNull(table);
    Objects.requireNonNull(magicCookie);

    if (magicCookie.length == 0) {
      throw new IllegalArgumentException("magicCookie must not be empty");
    }

    this.table = table;
    this.magicCookie = Arrays.copyOf(magicCookie, magicCookie.length);
  }

  /** {@inheritDoc} */
  @SuppressWarnings("unchecked")
  @Override
  public void actionPerformed(ActionEvent event) {
    var chooser = new JFileChooser();
    chooser.setSelectedFile(new File("data.dat"));
    chooser.setFileFilter(new FileFilter() {

      @Override
      public boolean accept(File file) {
        return file.getName().toLowerCase(Locale.getDefault()).endsWith("dat");
      }

      @Override
      public String getDescription() {
        return "Data file";
      }

    });
    var returnValue = chooser.showSaveDialog(null);
    if (returnValue != JFileChooser.APPROVE_OPTION) {
      return;
    }

    var model = (CustomTableModel<E>) table.getModel();
    var file = chooser.getSelectedFile();
    try (var fos = new ObjectOutputStream(new FileOutputStream(file))) {
      fos.write(magicCookie);
      for (var index = 0; index < model.getRowCount(); index++) {
        fos.writeObject(model.getEntryAt(index));
      }

      var msg = "Successfully exported all table data.";
      LOGGER.log(Level.INFO, msg);
      var frame = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, table);
      JOptionPane.showMessageDialog(frame, msg, "Success", JOptionPane.INFORMATION_MESSAGE);
    }
    catch (IOException e) {
      var msg = "An error was encountered during export: ";
      LOGGER.log(Level.SEVERE, msg + e.getMessage());
      UiUtils.displayErrorMessage(null, "Error", msg + "\n" + e.getMessage());
    }
  }

}
