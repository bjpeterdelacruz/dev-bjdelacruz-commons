package dev.bjdelacruz.commons.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.SystemColor;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * A status bar that displays help messages or the current status of an application.
 * 
 * @author BJ Peter Dela Cruz
 */
public class AppStatusBar extends JPanel {

  private final JPanel innerPanel = new JPanel(new BorderLayout());

  /**
   * Creates a new StatusBar.
   */
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public AppStatusBar() {
    setLayout(new BorderLayout());
    setPreferredSize(new Dimension(10, 23));
    setBackground(SystemColor.control);
    innerPanel.setOpaque(false);
    add(innerPanel, BorderLayout.CENTER);
  }

  /**
   * Adds a component to the status bar.
   * 
   * @param component The component to add to the status bar.
   * @param position The position to place the component.
   */
  public void addComponent(JComponent component, String position) {
    if (position == null || position.isEmpty()) {
      throw new IllegalArgumentException("position must not be null or empty");
    }
    innerPanel.add(component, position);
  }

  /** {@inheritDoc} */
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);

    g.setColor(new Color(0, 0, 0));
    g.drawLine(0, 0, getWidth(), 0);
  }

}
