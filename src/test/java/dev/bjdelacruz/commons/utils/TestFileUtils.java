package dev.bjdelacruz.commons.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Properties;

/**
 * Unit tests for the {@link FileUtils} class.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public class TestFileUtils {

  private static final String HELLO = "hello";
  private transient Path file;

  /**
   * Creates the file used for a test.
   * 
   * @throws IOException If there are problems creating the temporary file.
   */
  @BeforeEach
  public void init() throws IOException {
    var tempDir = new File(System.getProperty("java.io.tmpdir")).toPath();
    file = Files.createTempFile(tempDir, ".temp", null);
  }

  /**
   * Deletes the file after a test is completed.
   * 
   * @throws IOException If there are problems deleting the temporary file.
   */
  @AfterEach
  public void cleanup() throws IOException {
    Files.delete(file);
  }

  /**
   * Tests the {@link FileUtils#writeToFile(List, File)} method.
   * 
   * @throws IOException If there are problems writing to the temporary file.
   */
  @Test
  public void testWriteToFile() throws IOException {
    var lines = List.of("line1", "line2", "line3");
    try (var is = new FileInputStream(file.toFile());
         var reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
      FileUtils.writeToFile(lines, file.toFile());
      assertThat(reader.readLine()).isEqualTo("line1");
      assertThat(reader.readLine()).isEqualTo("line2");
      assertThat(reader.readLine()).isEqualTo("line3");
    }
  }

  /**
   * Tests the {@link FileUtils#loadProperties(Properties, File)} method.
   * 
   * @throws IOException If there are problems saving and loading the properties.
   */
  @Test
  public void testLoadProperties() throws IOException {
    FileUtils.saveProperty(HELLO, "worlds", file.toFile());
    FileUtils.saveProperty("foo", "bar", file.toFile());
    FileUtils.saveProperty("hi", "bye", file.toFile());
    var properties = new Properties();
    assertThat(properties).isEmpty();
    FileUtils.loadProperties(properties, file.toFile());
    assertThat(properties.size()).isEqualTo(3);
    assertThat(properties.getProperty(HELLO)).isEqualTo("worlds");
    assertThat(properties.getProperty("foo")).isEqualTo("bar");
    assertThat(properties.getProperty("hi")).isEqualTo("bye");
  }

  /**
   * Tests the {@link FileUtils#saveProperty(String, String, File)} method.
   * 
   * @throws IOException If there are problems saving and loading the properties.
   */
  @Test
  public void testSaveProperty() throws IOException {
    FileUtils.saveProperty(HELLO, "world", file.toFile());
    var result = FileUtils.getProperty(HELLO, file.toFile());
    assertThat(result.orElseThrow()).isEqualTo("world");
  }

  /**
   * Tests the {@link FileUtils#getIntProperty(String, File)} method.
   * 
   * @throws IOException If there are problems saving and loading the properties.
   */
  @Test
  public void testGetIntProperty() throws IOException {
    FileUtils.saveProperty("number", "1", file.toFile());
    assertThat(FileUtils.getIntProperty("number", file.toFile())).isEqualTo(1);
    FileUtils.saveProperty("string", "invalid", file.toFile());
    assertThat(FileUtils.getIntProperty("string", file.toFile())).isEqualTo(0);
    assertThat(FileUtils.getIntProperty("invalid", file.toFile())).isEqualTo(0);
  }

  /**
   * Tests the {@link FileUtils#getBooleanProperty(String, File)} method.
   * 
   * @throws IOException If there are problems saving and loading the properties.
   */
  @Test
  public void testGetBooleanProperty() throws IOException {
    FileUtils.saveProperty("true", "true", file.toFile());
    assertThat(FileUtils.getBooleanProperty("true", file.toFile())).isTrue();
    FileUtils.saveProperty("false", "false", file.toFile());
    assertThat(FileUtils.getBooleanProperty("false", file.toFile())).isFalse();
    assertThat(FileUtils.getBooleanProperty("invalid", file.toFile())).isFalse();
  }

}
