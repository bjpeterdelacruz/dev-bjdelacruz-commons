package dev.bjdelacruz.commons.ui;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Image;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.HyperlinkEvent;

/**
 * A dialog box that displays information about an application.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public final class AppAboutDialog extends JDialog {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /**
   * Creates a new About dialog box.
   * 
   * @param config The configuration for this dialog box (e.g. title, version, copyright, etc.).
   * @param aboutIcon The image icon that will be displayed in the panel.
   * @param windowIcon The image icon for this window.
   * @param parent The parent window for this dialog box.
   */
  @SuppressFBWarnings("VA_FORMAT_STRING_USES_NEWLINE")
  @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
  public AppAboutDialog(AboutDialogConfig config, ImageIcon aboutIcon, Image windowIcon, Component parent) {
    setTitle(config.getTitle());
    setModal(true);
    setLayout(new BorderLayout());

    var centerPanel = new JPanel(new BorderLayout());
    var iconLabel = new JLabel();
    if (aboutIcon != null) {
      iconLabel.setIcon(aboutIcon);
    }
    iconLabel.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 0));
    centerPanel.add(iconLabel, BorderLayout.WEST);
    var html = String.format("""
    <html>
      <span style="font-family: Arial; font-size: 12pt">
        %s
        <br>
        %s
        <br>""", config.getVersion(), config.getCopyright());
    if (config.getWebsite() != null && !config.getWebsite().isEmpty()) {
      html += String.format("""
        <br>
        Website: <a href="%s" style="text-decoration: none">%s</a>""", config.getWebsite(),
              config.getWebsite());
    }
    if (config.getEmail() != null && !config.getEmail().isEmpty()) {
      html += String.format("""
        <br>
        E-mail: <a href="mailto:%s" style="text-decoration: none">%s</a>""", config.getEmail(),
              config.getEmail());
    }
    html += """
      </span>
    </html>""";
    var infoPane = new JEditorPane("text/html", html);
    infoPane.setEditable(false);
    infoPane.setOpaque(false);
    infoPane.addHyperlinkListener(event -> {
        if (HyperlinkEvent.EventType.ACTIVATED.equals(event.getEventType())) {
          try {
            Desktop.getDesktop().browse(event.getURL().toURI());
          }
          catch (IOException | URISyntaxException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
          }
        }
    });
    infoPane.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
    centerPanel.add(infoPane, BorderLayout.CENTER);

    var okButton = new JButton("OK");
    okButton.addActionListener(_ -> dispose());
    var southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    southPanel.add(okButton);

    add(centerPanel, BorderLayout.CENTER);
    add(southPanel, BorderLayout.SOUTH);
    pack();
    setLocationRelativeTo(parent);
    if (windowIcon != null) {
      setIconImage(windowIcon);
    }
  }

}
