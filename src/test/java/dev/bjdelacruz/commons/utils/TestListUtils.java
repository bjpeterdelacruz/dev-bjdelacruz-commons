package dev.bjdelacruz.commons.utils;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Unit tests for the {@link ListUtils} class.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
public class TestListUtils {

  /**
   * Tests the {@link ListUtils#integersListToString(List, boolean)} method.
   */
  @Test
  public void testListToString() {
    assertThat(ListUtils.integersListToString(null, true)).isEmpty();
    assertThat(ListUtils.integersListToString(new ArrayList<Integer>(), true)).isEmpty();

    var output = ListUtils.integersListToString(List.of(1, 3, 5, 7, 8, 9, 11, 13, 14), true);
    assertThat(output).isEqualTo("1, 3, 5, 7-9, 11, 13-14");
    output = ListUtils.integersListToString(List.of(1, 3, 5, 7, 8, 9, 11, 13, 14), false);
    assertThat(output).isEqualTo("1, 3, 5, 7, 8, 9, 11, 13, 14");
    output = ListUtils.integersListToString(List.of(1, 2, 3, 4, 5, 6), true);
    assertThat(output).isEqualTo("1-6");
    output = ListUtils.integersListToString(List.of(1, 3, 5, 7, 9, 11), true);
    assertThat(output).isEqualTo("1, 3, 5, 7, 9, 11");
  }

  /**
   * Tests the {@link ListUtils#createSet(Object...)} method.
   */
  @Test
  public void testCreateSet() {
    assertThatThrownBy(() -> ListUtils.createSet((Object[]) null))
            .isInstanceOf(IllegalArgumentException.class);

    assertThat(ListUtils.createSet()).isEmpty();
    var set = new HashSet<>(List.of("haruna", "hiei", "kirishima", "kongou"));
    assertThat(ListUtils.createSet("kongou", "hiei", "haruna", "kirishima")).isEqualTo(set);
  }

  /**
   * Tests the {@link ListUtils#createIntegersList(int, int)} method.
   */
  @Test
  public void testCreateIntegersList() {
    assertThatThrownBy(() -> ListUtils.createIntegersList(10, 1))
            .isInstanceOf(IllegalArgumentException.class);
    assertThat(ListUtils.createIntegersList(1, 10)).isEqualTo(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
  }

}
