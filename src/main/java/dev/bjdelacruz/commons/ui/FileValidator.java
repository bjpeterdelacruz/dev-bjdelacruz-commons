package dev.bjdelacruz.commons.ui;

import java.io.File;

/**
 * A validator that can be used to check whether a file is valid based on some condition.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
@FunctionalInterface
public interface FileValidator {

  /**
   * Returns true if the given file is valid, or false otherwise.
   * 
   * @param file The file to check for validity.
   * @return True if the given file is valid, or false otherwise.
   */
  boolean isValidFile(File file);

}
