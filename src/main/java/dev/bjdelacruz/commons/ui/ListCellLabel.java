package dev.bjdelacruz.commons.ui;

/**
 * All Label objects must have a display name for readability.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 */
@FunctionalInterface
public interface ListCellLabel {

  /**
   * The display name for this object.
   * 
   * @return The display name.
   */
  String getDisplayName();

}
