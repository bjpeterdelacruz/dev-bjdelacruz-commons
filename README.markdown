# dev-bjdelacruz-commons

This Java library contains:

* Custom Swing GUI components
* Utility classes for files, lists, strings, etc.
* Methods for interpolating data for graphs.

## User Guide

1. Download the `dev-bjdelacruz-commons.jar` file from [this Amazon S3 bucket](https://dev-bjdelacruz-commons.s3.us-west-2.amazonaws.com/dev-bjdelacruz-commons.jar).

2. Add it as a dependency in your Gradle build file:

```
dependencies {
    implementation files('dev-bjdelacruz-commons.jar')
}
```

## Developer Guide
1. Install the following:
    * Java 23 JDK or higher

2. Run `./gradlew clean build` to compile and assemble the `dev-bjdelacruz-commons.jar` file.

## Links
[Developer's Website](https://bjdelacruz.dev)

[Online Documentation](https://bjdelacruz.dev/java/dev-bjdelacruz-commons/index.html)
