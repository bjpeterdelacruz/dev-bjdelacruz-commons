package dev.bjdelacruz.commons.utils.math;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.Objects;

/**
 * A data point for a JFreeChart.
 * 
 * @author BJ Peter Dela Cruz (bj.peter.delacruz@gmail.com)
 * 
 * @param <X> A Number representing an X-axis value.
 * @param <Y> A Number representing a Y-axis value.
 */
@SuppressFBWarnings("PI_DO_NOT_REUSE_PUBLIC_IDENTIFIERS_CLASS_NAMES")
public class Point<X extends Number, Y extends Number> {

  private final X x;
  private final Y y;

  /** Represents no data for a domain or range value. */
  public static final Number NO_DATA = Double.NaN;

  /**
   * Creates a Point object that can be used to represent a data point on a JFreeChart.
   * 
   * @param x The X-axis value.
   * @param y The Y-axis value.
   */
  public Point(X x, Y y) {
    Objects.requireNonNull(x);
    Objects.requireNonNull(y);

    this.x = x;
    this.y = y;
  }

  /**
   * Creates a Point object from an existing Point object.
   * 
   * @param point The Point object whose x- and y-values are to be copied to this object.
   */
  public Point(Point<X, Y> point) {
    x = point.x;
    y = point.y;
  }

  /**
   * Returns the value for X.
   *
   * @return The value for X.
   */
  public X getX() {
    return x;
  }

  /**
   * Returns the value for Y.
   *
   * @return The value for Y.
   */
  public Y getY() {
    return y;
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object obj) {
    return obj instanceof Point<?, ?> point && x.equals(point.x) && y.equals(point.y);
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

}
